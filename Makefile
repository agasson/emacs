clean:
	@rm -f init.elc init.el early-init.el early-init.elc

compile: README.org clean
	@emacs -Q --batch -l 'src/compile.el'
